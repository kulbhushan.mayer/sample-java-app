# sample-java-app
This is Sample Java Application
### Toold required on build machine for compilation & packaging
1. maven 3.5.4
2. Git 2.27+
3. JDK 1.8.0_161
### Check tools version using below commands on CMD, GIT BASH or PUTTY
1. mvn --version
```
Apache Maven 3.5.4 (1edded0938998edf8bf061f1ceb3cfdeccf443fe; 2018-06-18T00:03:14+05:30)
Maven home: C:\tools\apache-maven-3.5.4-bin
Java version: 1.8.0_161, vendor: Oracle Corporation, runtime: C:\tools\jdk1.8.0_161\jre
Default locale: en_IN, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```
2. git --version
```
git version 2.30.0.windows.2
```
3. java --version
```
java version "1.8.0_161"
Java(TM) SE Runtime Environment (build 1.8.0_161-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.161-b12, mixed mode)
```
### Topics to be discussed
1. Squash Merge
2. SonarQube Integration
3. Install Jenkins
4. Setup Pipeline
5. Install & Overview or UCD
