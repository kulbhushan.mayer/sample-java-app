provider "aws"{
  region = "us-east-2"
}
variable "server"{}
variable "type"{}
resource "aws_instance" "server"{
  ami = "ami-00399ec92321828f5"
  key_name = "kul"
  instance_type = var.type
  tags = {
    Name = "kul"
  }
  count = var.server
}
output "public" {
  value = aws_instance.server.*.public_ip
}